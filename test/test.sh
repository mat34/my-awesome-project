#!/bin/bash
sleep 15
URL=$URL

echo "TEST PING"
RESPONSE=$(curl -sb -H "$URL/ping" | jq -r '.message')
echo "Reponse ping :"
echo $RESPONSE
PINGMODEL="pong"
echo "Reponse attendue :"
echo $PINGMODEL
if [ "$RESPONSE" = "$PINGMODEL" ]
then 
  echo "la fonction ping retourne pong"
else 
  echo "la fonction ping ne retourne pas pong"
fi

curl -sb -H "$URL/ping"
curl -sb -H "$URL/ping"
curl -sb -H "$URL/ping"

echo "Test count"
COUNTRESPONSE=$(curl -sb -H "$URL/count" | jq -r '.pingCount')
echo "Reponse count :"
echo $COUNTRESPONSE
COUNTMODEL="4"
echo "Reponse attendue :"
echo $COUNTMODEL
if [ "$COUNTRESPONSE" = "$COUNTMODEL" ]
then 
  echo "Le compteur est bon"
else 
  echo "Le compteur n'est pas bon"
fi
